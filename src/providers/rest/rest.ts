import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { Http, Response } from '@angular/http';
// import { HttpClient, Response } from '@angular/common/http';
import 'rxjs/add/operator/map';

// CLASSE PRODOTTO
export class Product {
  id: number;
  prodname: string;
  proddesc: string;
  prodprice: number;
  prodimg: string;

  constructor(id, name, desc, price, img) {
    this.id = id;
    this.prodname = name;
    this.proddesc = desc;
    this.prodprice = price;
    this.prodimg = img;
  }
}

// CLASSE CARRELLO
export class Cart {
  name: string;
  note: string;
  total: number;
  rows: CartRow[] = [];
}

// CLASSE PER RIGA CARRELLO
export class CartRow {
  product: Product;
  qty: number;
}

// IMPOSTO COSTANTE - URL
const BASE_URL = 'http://localhost:8080';

@Injectable()
export class RestProvider {

  constructor(public http: Http) {
    console.log('Hello RestProvider Provider');
  }

  // ISTANZIO IL CARRELLO
  cart: Cart = new Cart();

  getCart() {
    return this.cart;
  }

  setName(name: string) {
    this.cart.name = name;
  }

  setNote(note: string) {
    this.cart.note = note;
  }

  getProductsList(): Observable<Response> {
    return this.http.get(BASE_URL + '/products');
  }

  getProduct(id: number): Observable<Product> {
    return this.http.get(BASE_URL + '/' + id)
    .map((response) => {
      return response.json();
    });
  }

  setProduct(id: number, product: Product): Observable<Product> {
    return this.http.post(BASE_URL + '/' + id, product)
    .map((response) => {
      return response.json();
    });
  }

  // TROVA PRODOTTO IN CARRELLO
  findInCart(product: Product): number {
    for (let i = 0; i < this.cart.rows.length; i++) {
      let row = this.cart.rows[i];
      if (row.product.id === product.id) {
        return i;
      }
    }
    return -1;
  }

  // AGGIUNGI PRODOTTO AL CARRELLO
  addToCart(product: Product) {
    let row_number = this.findInCart(product);
    if (row_number >= 0) {
      let row = this.cart.rows[row_number];
      row.qty += 1;
    } else {
      let row = new CartRow();
      row.product = product;
      row.qty = 1;
      this.cart.rows.push(row);
    }
    this.totalCart();
  }

  // RIMUOVI DAL CARRELLO
  deleteFromCart(product: Product) {
    let row_number = this.findInCart(product);
    if (row_number >= 0) {
      let row = this.cart.rows[row_number];
      if (row.qty > 1) {
        row.qty -= 1;
      } else {
        this.cart.rows.splice(row_number);
      }
    }
    this.totalCart();
  }

  //INVIA ORDINE
  sendOrder(): Observable<boolean> {
    return this.http.post(BASE_URL + '/sendOrder', this.cart)
      .map((response) => {
        let ok = response.text();
        if (ok === 'OK') {
          this.cart.rows = [];
          this.cart.total = 0;
          return true;
        } else {
          return false;
        }
      });
  }

  // CALCOLO TOTALE CARRELLO
  totalCart() {
    let total = 0;
    for (let i = 0; i < this.cart.rows.length; i++) {
      let row = this.cart.rows[i];
      total = total + row.product.prodprice * row.qty;
    }
    this.cart.total = total;
  }
}
