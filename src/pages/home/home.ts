import { RestProvider, Product, Cart } from './../../providers/rest/rest';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  topbar_heading:string = "Piadineria ENAIP";

  hero_header:string = "Benvenuto/a alla Piadineria ENAIP!";

  // imgPath:string = "../../assets/imgs/products/";

  productList: Product[] = [
    new Product(1, 'prodotto 1', 'descrizione 1', 30, 'nomeImg'),
    new Product(2, 'prodotto 2', 'descrizione 2', 50, 'nomeImg'),
    new Product(3, 'prodotto 3', 'descrizione 3', 34, 'nomeImg'),
    new Product(4, 'prodotto 4', 'descrizione 4', 311, 'nomeImg'),
    new Product(5, 'prodotto 5', 'descrizione 5', 335, 'nomeImg'),
    new Product(6, 'prodotto 6', 'descrizione 6', 3, 'nomeImg')
  ];

  product: Product = null;

  cart: Cart = null;

  constructor(public navCtrl: NavController, public restProvider: RestProvider) {

    this.cart = this.restProvider.getCart();
  }

  onAddProduct(p: Product) {
    this.restProvider.addToCart(p);
  }
  onRemoveProduct(p: Product) {
    this.restProvider.deleteFromCart(p);
  }
}
