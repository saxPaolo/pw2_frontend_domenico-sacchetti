import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  topbar_heading:string = "Chi siamo";

  constructor(public navCtrl: NavController) {

  }

}
